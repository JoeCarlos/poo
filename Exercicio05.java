package exerciciosPOO;
import java.util.Scanner;
public class Exercicio05 {
public static void main(String[] args) {
	float comp, larg, h;
	double f;
	int cx, in;
	Scanner tec = new Scanner(System.in);
	System.out.println("Digite o comprimento da cozinha? ");
	comp = tec.nextFloat();
	System.out.println("Qual a largura da cozinha? ");
	larg = tec.nextFloat();
	System.out.println("Qual a altura da cozinha? ");
	h = tec.nextFloat();
	in = (int)(((comp*h*2) + (larg*2*h))/1.5);
	f = (((comp*h*2) + (larg*2*h))/1.5) - in;
	if (f != 0) {
		cx = ++in;
	}
	else {
		cx = in;
	}
	System.out.println("Quantidade de caixas de azulejos para colocar em todas as\r\n" + 
			"paredes: " + cx);
	tec.close();
}

}
