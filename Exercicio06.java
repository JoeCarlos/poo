package exerciciosPOO;

import java.util.Scanner;

public class Exercicio06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float kmi, kmf, comb, rs, cons;
		double luc;
		Scanner tec = new Scanner (System.in);
		System.out.println("Marca��o do od�metro no in�cio do dia: ");
		kmi = tec.nextFloat();
		System.out.println("Marca��o do od�metro no fim do dia: ");
		kmf = tec.nextFloat();
		System.out.println("Quantidade de litros de combust�vel gastos: ");
		comb = tec.nextFloat();
		System.out.println("O total(R$) recebido dos passageiros: ");
		rs = tec.nextFloat();
		cons = (kmf-kmi)/comb;
		luc = rs - (comb*1.9);
		System.out.println("M�dia do consumo: " + cons + "Km/L " + "Lucro l�quido: R$" + luc);
		tec.close();
	}

}
