package exerciciosPOO;

import java.util.Scanner;

public class Exercicio02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tec = new Scanner(System.in);
		float temf, tempc;
		System.out.println("Digite a temperatura em Fahrenheit: ");
		temf = tec.nextFloat();
		tempc= ((temf-32)*5)/9;
		System.out.printf("A temperatura em graus Celsius �: %5.1f \n", tempc);
		tec.close();
	}

}
