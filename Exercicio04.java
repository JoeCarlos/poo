package exerciciosPOO;

import java.util.Scanner;

public class Exercicio04 {

	public static void main(String[] args) {
		Scanner tec = new Scanner (System.in);
		float pot, larg, comp;
		int lampada;
		System.out.println("Digite a pot�ncia da lampada (em watts): ");
		pot = tec.nextFloat();
		System.out.println("Digite agora a largura e o comprimento do c�modo (em metros): ");
		larg = tec.nextFloat();
		comp = tec.nextFloat();
		tec.close();
		lampada =(int) ((larg*comp*18)/pot);
		System.out.println("A quantidade de l�mpadas de pot�ncia " + pot + "W necess�rias para "
				+ "iluminar esse c�modo �: " + lampada);
	}

}
