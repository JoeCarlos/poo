package exerciciosPOO;
import java.util.Scanner;
public class Exercicio07 {
public static void main(String[] args) {
	float pista, cons, litros, comb_total;
	int voltas, reab;
	Scanner tec = new Scanner(System.in);
	System.out.print("Comprimento da pista (em metros): ");
	pista = tec.nextFloat();
	System.out.print("\nN�mero de voltas: ");
	voltas = tec.nextInt();
	System.out.print("\nN�mero de abastecimentos desejados: ");
	reab = tec.nextInt();
	System.out.print("\nConsumo de combust�vel do carro(Km/L): ");
	cons = tec.nextFloat();
	 comb_total = (((pista/1000)*voltas)/cons);
	 litros = comb_total/reab;
	 System.out.println("M�nimo de combust�vel necess�rio at� o primeiro abastecimento"
	 		+ " �: " + litros + "L");
	tec.close();
}
}
