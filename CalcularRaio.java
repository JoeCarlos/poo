package exerciciosPOO;

import java.util.Scanner;

public class CalcularRaio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner teclado = new Scanner(System.in);
		System.out.print("Digite o raio do c�rculo: ");
		double r = teclado.nextDouble();
		double area = Math.PI*(r*r);
		System.out.printf("A �rea desse c�rculo �: %6.2f",  area );
		teclado.close();
	}
		
}
