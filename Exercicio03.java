package exerciciosPOO;

import java.util.Scanner;

public class Exercicio03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tec = new Scanner(System.in);
		float temf, tempc;
		System.out.println("Digite a temperatura em Celsius: ");
		tempc = tec.nextFloat();
		temf = (tempc*9)/5+32;
		tec.close();
		System.out.printf("A temperatura em Fahrenheit �: %5.1f\n", temf);
	}

}
